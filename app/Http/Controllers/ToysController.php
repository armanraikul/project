<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Toy;

class ToysController extends Controller
{
    public function index()
    {
        $toys = Toy::all();
        return view('main.index', ['toys' => $toys]);
    }

    public function show($id)
    {
        $toys = Toy::find($id);
        return view('main.show', ['toys' => $toys]);
    }
}
