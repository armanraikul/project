<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
class FeedbacksController extends Controller
{
    public function update(Request $request, $name){
        $feedbacks = new Feedback;
        $feedbacks->grade = $request->get('grade');
        $feedbacks->feedback = $request->get('feedback');
        $feedbacks->name = $name;
        $feedbacks->save();
        return redirect()->route('toys.index');
    }
}
