<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
    box-sizing: border-box;
}

/* Create four equal columns that floats next to each other */
.column {
    float: left;
    width: 25%;
    padding: 10px;
    height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}
</style>
</head>
<body>

<h2>Info about {{ $toys->name }}</h2>

<div class="row">
  <div class="column" style="background-color:#aaa;">
    <h2>Name</h2>
    <p>{{ $toys->name }}</p>
  </div>
  <div class="column" style="background-color:#bbb;">
    <h2>Price</h2>
    <p>{{ $toys->price }}</p>
  </div>
  <div class="column" style="background-color:#ccc;">
    <h2>Number of items</h2>
    <p>{{ $toys->count }}</p>
  </div>
  <div class="column" style="background-color:#ddd;">
    <h2>Arrival Date</h2>
    <p>{{ $toys->arrival }}</p>
  </div>

     <div class="column" style="background-color:#ddd;">
         <form action="{{route('feedbacks.update', $toys->name)}}">
    <h2>Grade:</h2>
    <p><select name="grade">
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
            <option value="5">5</option>

  </select></p>
         <h2>Feedback:</h2>
    <p><textarea name="feedback"></textarea></p>
             <button type="submit">Submit</button>
              </form>
  </div>

</div>

</body>
