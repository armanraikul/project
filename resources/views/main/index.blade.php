<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    width: 100%;
    border: 1px solid #ddd;
}

th, td {
    text-align: left;
    padding: 16px;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}
</style>
</head>

<table>
  <tr>
    <th>ID</th>
    <th>Name</th>
    <th></th>
  </tr>
    @foreach($toys as $toy)
  <tr>
    <td>{{ $toy->id }}</td>
    <td>{{ $toy->name }}</td>
    <td><a href="{{ route('toys.show', $toy->id) }}">
            Select
        </a></td>
  </tr>
  @endforeach
</table>

